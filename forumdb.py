#
# Database access functions for the web forum.
# 
import bleach
import time
import psycopg2
## Database connection


## Get posts from database.
def GetAllPosts():
    '''Get all the posts from the database, sorted with the newest first.

    Returns:
      A list of dictionaries, where each dictionary has a 'content' key
      pointing to the post content, and 'time' key pointing to the time
      it was posted.
    '''
    DB = psycopg2.connect("dbname=forum")
    cursor = DB.cursor()
    cursor.execute("""SELECT content, time FROM posts order by time desc;""")
    result = cursor.fetchall()
    posts = [{'content': str(row[0]), 'time': str(row[1])} for row in result]
    DB.close()
    return posts

## Add a post to the database.
def AddPost(content):
    '''Add a new post to the database.

    Args:
      content: The text content of the new post.
    '''
    DB = psycopg2.connect("dbname=forum")
    cursor = DB.cursor()
    t = time.strftime('%c', time.localtime())
    cleaned = bleach.clean(content)
    cursor.execute("""INSERT INTO posts VALUES(%s);""", (cleaned, ))
    DB.commit()
    DB.close()